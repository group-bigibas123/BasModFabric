package com.github.bigibas123.basmodfabric.utils;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class FormatUtils {
    private static final DecimalFormat df;
    static {
        df = new DecimalFormat();
        df.setRoundingMode(RoundingMode.CEILING);
        df.setMinimumFractionDigits(2);
        df.setMaximumFractionDigits(2);
        df.setMinimumIntegerDigits(0);
        df.setDecimalSeparatorAlwaysShown(true);
        df.setGroupingUsed(false);
    }

    public static String formatDouble(double doub){
        return df.format(doub);
    }

}
