package com.github.bigibas123.basmodfabric.mixin;

import com.github.bigibas123.basmodfabric.utils.FormatUtils;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.hud.InGameHud;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import static net.minecraft.client.gui.DrawableHelper.fill;

@Mixin( InGameHud.class )
@Environment( EnvType.CLIENT )
public abstract class IngameOverLayMixin {


    @Shadow
    @Final
    private MinecraftClient client;

    @Shadow
    public abstract TextRenderer getFontRenderer();

    @Shadow
    protected abstract PlayerEntity getCameraPlayer();

    private int count;

    @Inject( at = @At( value = "INVOKE", target = "Lnet/minecraft/client/gui/hud/InGameHud;renderStatusEffectOverlay()V" ), method = "render" )
    private void renderGui(CallbackInfo info) {
        count = 0;
        if (!this.client.options.hudHidden && !this.client.options.debugEnabled) {

            drawText("(" +
                    FormatUtils.formatDouble(this.getCameraPlayer().getX()) + ";" +
                    FormatUtils.formatDouble(this.getCameraPlayer().getY()) + ";" +
                    FormatUtils.formatDouble(this.getCameraPlayer().getZ()) + ")");
            Identifier dim = Registry.DIMENSION_TYPE.getId(this.getCameraPlayer().getEntityWorld().getDimension().getType());
            drawText(dim != null ? dim.getPath() : String.valueOf(this.getCameraPlayer().getEntityWorld().getDimension().getType().getRawId()));
            drawText("H:(" + this.getCameraPlayer().getHungerManager().getFoodLevel() + "/20) S:(" + this.getCameraPlayer().getHungerManager().getSaturationLevel() + "/20)");
        }
    }

    private void drawText(String string) {
        int j = 9;
        int k = this.getFontRenderer().getStringWidth(string);
        int m = 2 + (10 * count++);
        fill(1, m - 1, 2 + k + 1, m + j - 1, -1873784752);
        this.getFontRenderer().draw(string, 2.0F, (float) m, 14737632);
    }

}
