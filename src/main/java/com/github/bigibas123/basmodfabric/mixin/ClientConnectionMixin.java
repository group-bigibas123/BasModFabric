package com.github.bigibas123.basmodfabric.mixin;


import com.sun.org.slf4j.internal.LoggerFactory;
import io.netty.buffer.Unpooled;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.impl.networking.CustomPayloadC2SPacketAccessor;
import net.fabricmc.fabric.impl.networking.PacketTypes;
import net.minecraft.network.ClientConnection;
import net.minecraft.network.Packet;
import net.minecraft.network.packet.c2s.play.CustomPayloadC2SPacket;
import net.minecraft.util.Identifier;
import net.minecraft.util.PacketByteBuf;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.logging.Logger;

@Mixin( ClientConnection.class )
@Environment( EnvType.CLIENT )
public abstract class ClientConnectionMixin {

    private final com.sun.org.slf4j.internal.Logger log = LoggerFactory.getLogger(this.getClass());


    @Shadow
    public abstract void send(Packet<?> packet, GenericFutureListener<? extends Future<? super Void>> callback);


    /*
    @Overwrite
    public void send(Packet<?> packet) {
        if ((packet instanceof CustomPayloadC2SPacket)) {
            CustomPayloadC2SPacketAccessor pack = ((CustomPayloadC2SPacketAccessor) packet);
            Identifier chan = pack.getChannel();
            Logger log = Logger.getLogger(this.getClass().getSimpleName());
            if (chan.equals(CustomPayloadC2SPacket.BRAND) || chan.equals(PacketTypes.BRAND)) {
                log.info("Replace Brand packet with vanilla");
                log.info("old Data:" + pack.getData().readString());
                this.send(new CustomPayloadC2SPacket(PacketTypes.BRAND, new PacketByteBuf(Unpooled.buffer()).writeString("vanilla")), null);
                return;
            } else if (chan.equals(PacketTypes.REGISTER)) {
                log.info("Blocked register packet");
                log.info("old Data:" + new String(pack.getData().array()));
                return;
            } else if (chan.equals(PacketTypes.UNREGISTER)) {
                log.info("Blocked unregister packet");
                log.info("old Data:" + new String(pack.getData().array()));
                return;
            }
        }
        this.send(packet, null);

    }
    */

    @Inject( at = @At( "HEAD" ), method = "send(Lnet/minecraft/network/Packet;)V", cancellable = true)
    public void onSend(Packet<?> packet, CallbackInfo callbackInfo) {
        if ((packet instanceof CustomPayloadC2SPacket)) {
            CustomPayloadC2SPacketAccessor pack = ((CustomPayloadC2SPacketAccessor) packet);
            Identifier chan = pack.getChannel();
            Logger log = Logger.getLogger(this.getClass().getSimpleName());
            if (chan.equals(CustomPayloadC2SPacket.BRAND) || chan.equals(PacketTypes.BRAND)) {
                log.fine("Replace Brand packet with vanilla");
                log.finer("old Data:" + pack.getData().readString());
                this.send(new CustomPayloadC2SPacket(PacketTypes.BRAND, new PacketByteBuf(Unpooled.buffer()).writeString("vanilla")), null);
                callbackInfo.cancel();
            } else if (chan.equals(PacketTypes.REGISTER)) {
                log.fine("Blocked register packet");
                log.finer("old Data:" + new String(pack.getData().array()));
                callbackInfo.cancel();
            } else if (chan.equals(PacketTypes.UNREGISTER)) {
                log.fine("Blocked unregister packet");
                log.finer("old Data:" + new String(pack.getData().array()));
                callbackInfo.cancel();
            }
        }
    }

}

